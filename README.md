# verticalELP

Making light detectors airworthy: The next logical step to measure ecological light pollution (ELP)

The project is currently being established. In the future, evaluation scripts, software and hardware will be published here.

## Nationalpark Berchtesgaden: Preliminary report (September 2021)  
Measurements in Berchtesgaden National Park at three sites located not far from each other but with different altitudes (630 to 2050 meters) and conditions (urban vs. remote).    

### Canon 6D with a 8 mm fisheye lense (New approach Upcycling)
Composed of 5 fisheye images (four images in the vertical plane and one towards the zenith). Taking five instead of one images results in a substantiate improved quality data at the horizon with the same hardware. 

#### Near Watzmannhaus
<a href="https://drive.google.com/file/d/1pAkX25y8Y-al3g6ZXRpQoxHzvZxzd6Kn/view">
  <img src="visuals/watzmann_B1small.jpg" alt="St. Bartholomä Konigssee">
</a> 
<img src="reports/Watzmann.jpg"  width="600" height="354"><img src="visuals/IMG_8378.jpg"  width="236" height="354">


#### St. Bartholomä 
<a href="https://drive.google.com/file/d/1_BlLGTqmOUXP4qWxXUOWIrFSWhwX9UBt/view">
  <img src="visuals/konigssee_B14small.jpg" alt="St. Bartholomä Konigssee">
</a> 
<img src="reports/Bartholoma.jpg"  width="600" height="354"> 



### Sony A7SII with a 24 mm linear lens mounted on a robotic panoramic head 
Composed of 28 images
#### Schönau am Königssee

<img src="reports/Schonau_am_Konigssee.jpg"  width="600" height="354">


## Manufacturing of a gimbal prototype (October 2021) 
Building a real-time gimbal prototype using only open source hardware and software.

![Beautiful to watch and very calming](visuals/20211018_155155.mp4)

![Christmas is coming: Making presents 4 kids](visuals/drehen.webm)

## First gimbal test (10.11.2021)
A Canon M200 with a Meike 6.5mm fisheye lens without image stabilization (IS) was mounted on a tripod (30sec exposure time). The orientation of the camera (pan and tilt) was either fixed or not fixed to test the image stabilization by the gimbal.
   
<img src="visuals/fix.JPG"  width="600" height="400">

- [x] Camera orientation fixed (pan and tilt)
- [ ] Gimbal IS 

<img src="visuals/IS_off.JPG"  width="600" height="400">

- [ ] Camera orientation fixed (pan and tilt)
- [ ] Gimbal IS 

<img src="visuals/IS_on.JPG"  width="600" height="400">

- [ ] Camera orientation fixed (pan and tilt)
- [x] Gimbal IS 
